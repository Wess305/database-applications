package com.example.demo;

public class ArtistDTO {
	private String Name;
	private String Bio;
	
	
	@Override
	public String toString() {
		return "ArtistDTO [Name=" + Name + ", Bio=" + Bio + "]";
	}


	public String getName() {
		return Name;
	}


	public void setName(String name) {
		Name = name;
	}


	public String getBio() {
		return Bio;
	}


	public void setBio(String bio) {
		Bio = bio;
	}
	
	
	
	

}
