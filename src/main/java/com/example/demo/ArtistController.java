package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/Artist")
public class ArtistController {

	@Autowired
	private ArtistRepository artistRepository;

	@GetMapping()
	public Iterable<Artist> findAll() {
		return artistRepository.findAll();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Artist create(@RequestBody Artist artist) {
		return artistRepository.save(artist);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id) {
		artistRepository.findById(id).orElseThrow(IllegalArgumentException::new);
		artistRepository.deleteById(id);
	}
	
	@PutMapping("/{name}")
	public Artist updateArtist(@RequestBody Artist artist, @PathVariable String name) {
		if (artist.getName() != name) {
			throw new IllegalArgumentException();
		}
		artistRepository.findByName(name).orElseThrow(IllegalArgumentException::new);
		return artistRepository.save(artist);


	}



}
